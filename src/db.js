const r = require('rethinkdb');
const http = require('http');

const config = require('./config/environment');
const Player = require('./models/player');

module.exports.createConnection = function*(next) {
  let conn;
  try {
    conn = yield r.connect(config.rethinkdb);
    this._rdbConn = conn;
  } catch (err) {
    this.status = 500;
    this.body = err.message || http.STATUS_CODES[this.status];
    this.level = 'db';
  }
  yield next;
};

module.exports.closeConnection = function*() {
  this._rdbConn.close();
};

r.connect(config.rethinkdb, (err, conn) => {
  if (err) {
    console.log('Could not open a connection to initialize the database');
    console.log(err.message);
    process.exit(1);
  }

  Player.createTable(conn);
  // TODO: fix here =>
  // .then (function(res) {console.log(res);}).error (function(err) {console.log(err, '____-');})
});
