const Player = require('../../models/player');
const Validators = require('../../models/validators');


module.exports.index = function*() {
  this.status = 403;
  this.body = {
    name: 'players',
  };
};

module.exports.getPlayerById = function*(next) {
  const params = this.params;
  const rdbConnection = this._rdbConn;
  const playerId = params.id;

  if (!playerId) {
    this.status = 422;
    // TODO: this wont work - change to some custom error handling and logging, but keeping f.e.
    console.error(new Error('missing player id', params, { level: 'players.controller' }));
    return yield next;
  }

  const result = yield Player.getPlayerById(playerId, rdbConnection);

  if (!result) {
    this.status = 404;
    this.body = {
      success: false,
      message: `player with id::${playerId} not found`,
      level: 'players.controller',
    };
    return this;
  }

  this.status = 200;
  this.body = JSON.stringify(result);
  return this;
};

module.exports.createPlayer = function*(next) {
  const body = this.request.body;
  const rdbConnection = this._rdbConn;

  if (!body) {
    this.status = 422;
    // TODO: this wont work - change to some custom error handling and logging, but keeping f.e.
    console.error(new Error('missing body', { level: 'players.controller' }));
    return yield next;
  }

  const playerValidator = yield Validators.playerValidator(body);
  const validationError = playerValidator.error;

  if (Validators.isValidationError(validationError)) {
    console.error(new Error('validation erorr'), playerValidator);

    this.status = 400;
    this.body = {
      success: false,
      message: 'validation error',
      reasons: validationError.details.map(detail => { return detail.message; }),
    };
    return this;
  }

  const player = playerValidator.value;
  if (!player) {
    this.status = 400;
    this.body = { success: false, message: 'wrong player' };
    return this;
  }

  const results = yield Player.createPlayer(player, rdbConnection);
  if (results) {
    if (results.errors > 0) {
      this.status = 400;
      this.body = { error: results.first_error.split(':')[0] };
      return this;
    }
    this.status = 204;
  } else {
    this.status = 422;
    return this;
  }
  yield next;
};

module.exports.removePlayerById = function*(next) {
  const params = this.params;
  const playerId = params.id;
  const rdbConnection = this._rdbConn;

  if (!playerId) {
    this.status = 422;
    // TODO: this wont work - change to some custom error handling and logging, but keeping f.e.
    console.error(new Error('missing player id', params, { level: 'players.controller' }));
    return yield next;
  }

  const results = yield Player.removePlayer(playerId, rdbConnection);
  if (results) {
    if (!results.errors) {
      this.status = 204;
    } else {
      console.error(new Error(`error when delete ${playerId}`));
    }
  } else {
    this.status = 422;
    return this;
  }
  yield next;
};

module.exports.updatePlayerById = function*(next) {
  const params = this.params;
  const update = this.request.body;
  const rdbConnection = this._rdbConn;
  const playerId = params.id;
  if (!playerId) {
    this.status = 422;
    // TODO: this wont work - change to some custom error handling and logging, but keeping f.e.
    console.error(new Error('missing player id', params, { level: 'players.controller' }));
    return yield next;
  }

  if (!update) {
    this.status = 422;
    // TODO: this wont work - change to some custom error handling and logging, but keeping f.e.
    console.error(new Error('missing update', { level: 'players.controller' }));
    return yield next;
  }

  yield Player.updatePlayer(playerId, update, rdbConnection);
  this.status = 204;
  yield next;
};
