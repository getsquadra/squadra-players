const Joi = require('joi');


module.exports.isValidationError = (err) => {
  if (!err) {return;}
  return err.name === 'ValidationError';
};

module.exports.playerValidator = (body) => {
  const schema = Joi.object().keys({
    _id: Joi.string().guid().required(),
    name: Joi.string().min(1).max(20).required(),
    surname: Joi.string().min(1).max(30).required(),
    birthyear: Joi.number().integer().min(1900).max(1999).required(),
    teams: Joi.array().min(0).max(5), /*premium can have > X teams [tech debt]*/
  }).with('surname', 'birthyear', 'teams');

  return Joi.validate(body, schema);
};
