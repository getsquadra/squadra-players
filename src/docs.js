import joi from 'joi';

const pkg = require('../package.json');

const Player = joi.object().label('Player').keys({
  _id: joi.string().required().description('Player ID'),
  name: joi.string().required().description('Player Name'),
  surname: joi.string().required().description('Player Surname'),
  birthyear: joi.string().required().description('Players date of birth'),
  teams: joi.string().required().description('Players teams'),
  createdAt: joi.date().required().description('Added Date'),
  updatedAt: joi.date().required().description('Last Updated Date'),
});

const NewPlayer = joi.object().label('Player').keys({
  _id: joi.string().required().description('Player ID'),
  name: joi.string().required().description('Player Name'),
  surname: joi.string().required().description('Player Surname'),
  birthyear: joi.string().required().description('Players date of birth'),
  teams: joi.string().required().description('Players teams'),
});

const UpdatedPlayer = joi.object().label('Player').keys({
  _id: joi.string().required().description('Player ID'),
  name: joi.string().required().description('Player Name'),
  surname: joi.string().required().description('Player Surname'),
  birthyear: joi.string().required().description('Players date of birth'),
  teams: joi.string().required().description('Players teams'),
});

const GetPlayer = {
  method: 'get',
  path: '/players/:id',
  meta: {
    friendlyName: 'Get Player by Id',
    description: 'Returns a player by id',
  },
  validate: {
    type: 'json',
    params: {
      _id: joi.string().description('Player ID').required(),
    },
    output: Player,
  },
  *handler() {},
};

const UpdatePlayer = {
  method: 'put',
  path: '/players/:id',
  meta: {
    friendlyName: 'Update Player by Id',
  },
  validate: {
    type: 'json',
    body: UpdatedPlayer,
    output: Player,
  },
  *handler() {},
};

const RemovePlayer = {
  method: 'delete',
  path: '/players/:id',
  meta: {
    friendlyName: 'Remove Player by Id',
  },
  validate: {
    type: 'json',
  },
  *handler() {},
};

const CreatePlayer = {
  method: 'post',
  path: '/players',
  meta: {
    friendlyName: 'Create Player',
  },
  validate: {
    type: 'json',
    body: NewPlayer,
    output: Player,
  },
  *handler() {},
};

const PlayersApi = {
  groupName: 'Players API',
  description: 'Functionality for dealing with players',
  routes: [
    GetPlayer,
    UpdatePlayer,
    RemovePlayer,
    CreatePlayer,
  ],
};

const docs = {
  title: pkg.name,
  version: pkg.version,
  theme: 'paper',
  groups: [
    PlayersApi,
  ],
};

export default docs;
