const path = require('path');
const _ = require('lodash');
require('dotenv').config({ silent: true });

const base = {
  env: process.env.NODE_ENV,
  root: path.normalize(`${__dirname}/../../..`),
  port: process.env.PORT || 9000,
  logType: 'dev',
  rethinkdb: {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    authKey: '',
    db: process.env.DB,
  },
};

module.exports = _.merge(base, require(`./${process.env.NODE_ENV}.js`) || {});
