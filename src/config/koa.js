const config = require('./environment');
const morgan = require('koa-morgan');
const koaDocs = require('koa-docs');
const bodyParser = require('koa-body-parser');

const db = require('../db');

import docs from '../docs';

module.exports = (app) => {
  app.use(morgan.middleware(config.logType));
  app.use(bodyParser());
  app.use(db.createConnection);
  app.use(koaDocs.get('/docs', docs));
};
