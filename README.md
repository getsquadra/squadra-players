[ ![Codeship Status for getsquadra/squadra-players](https://codeship.com/projects/33f7eb90-f81f-0133-ad4d-1a6ec51e4f6f/status?branch=master)](https://codeship.com/projects/150818)


## ENVIRONMENT: ##
Before all create `.env` file with your env variables with content:
```
DB_HOST=localhost
DB_PORT=28015
DB=squadra_players
```

## DATABASE: ##
install rethinkdb:
```sh
$ brew install rethinkdb
```

and run:
```sh
rethinkdb
```


Import some test data:
-- read https://rethinkdb.com/docs/importing/
```sh
$ rethinkdb import -f players.json --table squadra-players.players
```


## USAGE: ##
- install :
```sh
$ npm install
```

- to start watch server run:
```sh
$ npm run watch:server
```

- tests: 
```sh
$ npm test
```

- some API calls you can find here: 
[postman examples](https://bitbucket.org/getsquadra/squadra-players/wiki/Api%20description%20%5Bpostman%20examples%5D)

## DOCS: ##
created with [koa-docs](https://www.npmjs.com/package/koa-docs)

[Docs](http://localhost:9000/docs) available on `/docs` route.



