const controller = require('./players.controller');
const router = require('koa-router')();

router.get('/', controller.index);
router.post('/players', controller.createPlayer);
router.get('/players/:id', controller.getPlayerById);
router.del('/players/:id', controller.removePlayerById);
router.put('/players/:id', controller.updatePlayerById);

module.exports = router.routes();
