module.exports = {
  ip: process.env.IP || undefined,
  port: process.env.PORT || 8000,
  logType: 'combined',
};
