module.exports = {
  ip: process.env.IP || undefined,
  port: process.env.PORT || 9000,
  logType: 'dev',
};
