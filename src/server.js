process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const config = require('./config/environment');

const app = require('koa')();

require('./config/koa')(app);
require('./config/routes')(app);
require('koa-qs')(app);

if (!module.parent) {
  app.listen(config.port, config.ip, () => {
    console.log('Koa server listening on %d, in %s mode', config.port, config.env);
  });
}

exports = module.exports = app;
