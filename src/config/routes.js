const mount = require('koa-mount');
const players = require('../services/root');

module.exports = (app) => {
  app.use(mount('/', players));
};
