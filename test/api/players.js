import server from '../../src/server';
import request from 'supertest-as-promised';
import assert from 'assert';

const app = server.listen();

describe('GET /', function(){
  it('should respond with 403', function(done){
    request(app)
      .get('/')
      .expect(403, done);
  });
});

describe('GET /players/:id', function(){
  it('should respond with 200', function(done){
    request(app)
      .get('/players/f5ee43a4-ab38-42fd-bfd9-288d60ad9c16')
      .expect(200, done);
  });

  it('should respond with 404', function(done){
    request(app)
      .get('/players/notExist')
      .expect(404, done);
  });
});

describe('PUT /players/:id', function(){
  it('should respond with 204', function(done){
    request(app)
      .put('/players/f5ee43a4-ab38-42fd-bfd9-288d60ad9c16')
      .send({
        name: 'Name',
        surname: 'Surname',
        birthyear: 1954,
      })
      .expect(204, done);
  });

  it('should respond with 422 [empty data]', function(done){
    request(app)
      .put('/players/f5ee43a4-ab38-42fd-bfd9-288d60ad9c16')
      .send()
      .expect(422, done);
  });
});

describe('DELETE /players/:id', function(){
  it('should respond with 204', function(done){
    request(app)
    .del('/players/f5ee43a4-ab38-42fd-bfd9-288d60ad9c16')
    .expect(204, done);
  });
});

describe('POST /players', function(){
  it('should respond with 204', function(done){
    request(app)
      .post('/players')
      .send({
        _id: 'd9b9a7e0-1695-11e6-aa44-11686173f023',
        name: 'Name',
        surname: 'Surname',
        birthyear: 1989,
        teams: [{
          _id: 1,
          name: 'Snakes',
        },
        {
          _id: 2,
          name: 'Toads',
        }],
      })
      .expect(204, done);
  });

  it('should respond with 422 if no body', function(done){
    request(app)
      .post('/players')
      .expect(422, done);
  });

  it('should respond with 400 [wrong guid]', function(){
    request(app)
      .post('/players')
      .send({
        _id: '42',
        name: 'Name',
        surname: 'Surname',
        birthyear: 1954,
        teams: [],
      })
      .expect(400)
      .then(function(res){
        const error = JSON.parse(res.error.text);

        assert.equal(error.message, 'validation error', '[validation error]');
        assert.equal(error.reasons[0], '"_id" must be a valid GUID', '[GUID test]');
      });
  });
});
