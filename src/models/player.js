const r = require('rethinkdb');
const http = require('http');
const _ = require('lodash');

const config = require('../config/environment');

const SETTINGS = {
  table: 'players',
};

module.exports.getPlayerById = (id, conn) => {
  try {
    const cursor = r.table(SETTINGS.table).get(id).run(conn);
    return cursor;
  } catch (e) {
    this.status = 500;
    this.body = e.message || http.STATUS_CODES[this.status];
    return this;
  }
};

module.exports.createPlayer = (player, conn) => {
  try {
    player.createdAt = r.now();
    const result = r.table(SETTINGS.table)
      .insert(player, { returnChanges: true })
      .run(conn);
    return result;
  } catch (e) {
    this.status = 500;
    this.body = e.message || http.STATUS_CODES[this.status];
    return this;
  }
};

module.exports.removePlayer = (id, conn) => {
  try {
    const result = r.table(SETTINGS.table).get(id).delete().run(conn);
    return result;
  } catch (e) {
    this.status = 500;
    this.body = e.message || http.STATUS_CODES[this.status];
    return this;
  }
};

module.exports.updatePlayer = (id, update, conn) => {
  try {
    const result = r.table(SETTINGS.table)
      .get(id)
      .update(update, { returnChanges: true })
      .run(conn);
    return result;
  } catch (e) {
    this.status = 500;
    this.body = e.message || http.STATUS_CODES[this.status];
    return this;
  }
};

module.exports.createTable = (conn) => {
  r.tableList().run(conn).then(tableNames => {
    if (!_.contains(tableNames, SETTINGS.table)) {
      r.table(SETTINGS.table)
        .indexWait('createdAt').run(conn).then(() => {
          console.log('Table and index are available');
        }).catch(() => {
        // The database/table/index was not available, create them
          r.dbCreate(config.rethinkdb.db).run(conn).finally(() => {
            return r.tableCreate(SETTINGS.table).run(conn);
          }).finally(() => {
            r.table(SETTINGS.table).indexCreate('createdAt').run(conn);
          }).finally(() => {
            r.table(SETTINGS.table).indexWait('createdAt').run(conn);
          }).then(() => {
            console.log('Table and index are available');
            conn.close();
          }).catch((err) => {
            if (err) {
              console.log('Could not wait for the completion of the index `players`');
              console.log(err);
              process.exit(1);
            }
            console.log('Table and index are available');
            conn.close();
          });
        });
    }
  });
};
